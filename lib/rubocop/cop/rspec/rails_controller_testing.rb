# frozen_string_literal: true

require 'rubocop-rspec'
require_relative 'base'

module Rubocop
  module Cop
    module RSpec
      # Flags the use of `assigns` and `render_template`
      # in controller and request specs.
      #
      # @example
      #   # bad
      #   ssh_key = create(:personal_key, user: user)
      #   get admin_credentials_path(filter: 'ssh_keys')
      #   expect(assigns(:credentials)).to match_array([ssh_key])
      #
      #   # good
      #   ssh_keys = create_list(:personal_key, user: user)
      #   get admin_credentials_path(filter: 'ssh_keys')
      #   expect(response.body).to include(ssh_key.title)
      #
      #   # bad
      #   expect(response).to render_template :show
      #
      #   # good
      #   expect(response.body).to include('Cool stuff')
      #
      # @see https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html#about-controller-tests
      # @see https://www.bigbinary.com/blog/changes-to-test-controllers-in-rails-5#reasons-for-removing-assigns-and-assert-template
      class RailsControllerTesting < Base
        MSG_TEMPLATE = "Avoid using `%{name}`. Instead, assert on observable outputs such as the response body. See https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html#about-controller-tests"

        RESTRICT_ON_SEND = %i[assigns render_template].freeze

        def on_send(node)
          add_offense(node, message: format(MSG_TEMPLATE, name: node.method_name))
        end
      end
    end
  end
end
