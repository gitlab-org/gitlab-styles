# frozen_string_literal: true

RSpec.shared_examples GroupInviteMembers do
  expect(assigns[:search_error_if_version_incompatible]).to be_falsey
end
