# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::Styles do
  it "has a version number" do
    expect(Gitlab::Styles::VERSION).not_to be_nil
  end
end
